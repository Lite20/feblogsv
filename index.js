const fs = require('fs');
const admin = require('firebase-admin');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const config = require('./config.json');
const serviceAccount = require('./service-account.json');

const app = express();

app.use(cors());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
})); // support encoded bodies

app.get('/api/ping', (req, res) => {
    res.send('pong!');
});

app.post('/api/sub', (req, res) => {
    admin.messaging().subscribeToTopic(req.body.token, 'blog')
        .then(function (response) {
            console.log("Successfully subscribed to topic:", response);
            res.json({
                s: 'suc'
            });
        })
        .catch(function (error) {
            console.log("Error subscribing to topic:", error);
            res.json({
                s: 'err'
            });
        });
});

app.post('/api/edit', (req, res) => {
    var post = {
        title: req.body.title,
        date: Date.now(),
        content: req.body.content
    };

    if (req.body.pass == config.pass) {
        fs.writeFile(
            config.root + '/posts/' + formatTitle(req.body.title) + '.json',
            JSON.stringify(post),
            function (err) {
                if (err) {
                    console.log(err);
                    res.json({
                        s: 'err'
                    });
                }

                var index = require(config.root + '/posts/postdex.json');
                index.push(formatTitle(req.body.title));

                fs.writeFile(
                    config.root + '/posts/postdex.json',
                    JSON.stringify(index),
                    function (err) {
                        if (err) {
                            console.log(err);
                            res.json({
                                s: 'err'
                            });
                        }

                        notify(req.body.title);
                        res.json({
                            s: 'suc'
                        });
                    }
                );
            }
        );
    } else {
        res.json({
            s: 'err'
        });
    }
});

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

app.listen(8080, () => console.log('Feblog server sailing on port 8080!'));

function notify(title) {
    var payload = {
        notification: {
            title: 'New Post!',
            body: title,
            click_action: 'https://mcfish.space/#' + formatTitle(title)
        }
    };


    admin.messaging().sendToTopic('blog', payload).catch(function (error) {
        console.log("Error sending message:", error);
    });
}

function formatTitle(title) {
    return title.replace(' ', '_').replace(/[^a-zA-Z0-9-_]/g, '');
}
